# Customer review app.

Plan for development:

- [x] Log in page (presentation only)
- [x] Simple http client for /login endpoint
- [x] Persisting JWT token in React context
- [x] Simple http client for /api/reviews endpoint
- [x] React router for application routes (/login and /reviews)
- [x] Storing responses from /api/reviews and /api/themes in local state of Feed component
- [x] Feed screen
- [x] Deploy to surge

Check out deployed version of project at http://raspy-cats.surge.sh/

## First steps

- Run `yarn install` to have all npm packages installed

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `yarn lint`

Runs eslint and prettier againts the project
