import React, { FunctionComponent } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import './App.css';
import LoginPage from './pages/Login';
import { FeedPage } from './pages/Feed';

const App: FunctionComponent = () => (
  <div className="root_container">
    <header className="header">
      Customer Feedback service
    </header>

    <BrowserRouter>
      <Switch>
        <Route path="/feed">
          <FeedPage />
        </Route>
        <Route path="/">
          <LoginPage />
        </Route>
      </Switch>
    </BrowserRouter>
  </div>
);

export default App;
