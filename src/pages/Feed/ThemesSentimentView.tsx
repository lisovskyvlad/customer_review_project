import React from 'react';
import { ReviewThemeType } from '../../types/review.types';
import { ThemeType } from '../../types/theme.types';

interface ThemesSentimentViewProps {
  reviewThemes: Array<ReviewThemeType>;
  themes: Array<ThemeType>;
}

const sentimentEmojie: Record<ReviewThemeType['sentiment'], string> = {
  "-1": '\u{1F4A9}',
  "0": "\u{1F610}",
  "1": "\u{01F680}",
};

const ThemesSentimentView = ({
  reviewThemes,
  themes
}: ThemesSentimentViewProps) => (
  <div>
    {reviewThemes.map((reviewTheme, idx) => (
      <div key={`review-${idx}`} className="reviewContainer">
        <span>
          {themes.find(theme => theme.id === reviewTheme.theme_id)?.name}
        </span>

        <span className="emojie">
          {sentimentEmojie[reviewTheme.sentiment]}
        </span>
      </div>
    ))}
  </div>
);

export default ThemesSentimentView;
