import React, { useState, useEffect } from 'react';
import { ReviewType } from '../../types/review.types'
import { ThemeType } from '../../types/theme.types'

import ThemesSentimentView from './ThemesSentimentView';

import callReviewsEndpoint from '../../httpClients/reviews';
import callThemeEndpoint from '../../httpClients/themes';

import { useAuth } from '../../contexts/Auth';

const FeedPage = () => {
  const auth = useAuth();
  const [reviews, setReviews] = useState<Array<ReviewType>>([]);
  const [themes, setThemes] = useState<Array<ThemeType>>([]);
  const [filterTheme, setFilterTheme] = useState<ThemeType['id'] | 'all'>('all');

  useEffect(() => {
    callReviewsEndpoint({
      token: auth.jwtToken,
      limit: 20,
      offset: 0
    }).then((response) => {
      setReviews(response.data);
    });

  }, [auth.jwtToken]);

  useEffect(() => {
    const themesIds = reviews.flatMap(review =>
      review.themes.map(theme => theme.theme_id)
    );
    const uniqueThemesIds = Array.from(new Set(themesIds));

    const themes = uniqueThemesIds.map(async (themesId) => {
      const response = await callThemeEndpoint({
        token: auth.jwtToken,
        id: themesId
      });

      return response.data;
    });

    Promise.all(themes).then(results => setThemes(results));
  }, [auth.jwtToken, reviews]);

  let filteredReviews = reviews;
  if (filterTheme !== 'all') {
    filteredReviews = reviews.filter(review => {
      const themesIds = review.themes.map(theme => theme.theme_id);
      return themesIds.indexOf(filterTheme) !== -1;
    });
  }

  return (
    <div>
      <label>
        Filter by theme:
        <select defaultValue={'all'} className="filterTheme"
                onChange={(event) => setFilterTheme(parseInt(event.currentTarget.value)) }>
          <option value={'all'}>All</option>
          {themes.map(theme => (
            <option key={theme.id} value={theme.id}>{theme.name}</option>
          ))}
        </select>
      </label>

      <table className="pure-table pure-table-bordered">
        <colgroup>
          <col span={1} style={{width: "70%"}} />
          <col span={1} style={{width: "30%"}} />
        </colgroup>

        <thead>
          <tr>
            <th>Comment text</th>
            <th>Theme - Sentiment</th>
          </tr>
        </thead>
        <tbody className="reviewsBody">
          {filteredReviews.map(review => (
            <tr key={review.id}>
              <td className="commentText">{review.comment}</td>
              <td>
                <ThemesSentimentView reviewThemes={review.themes} themes={themes} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
};

export default FeedPage;
