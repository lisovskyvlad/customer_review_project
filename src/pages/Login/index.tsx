import React from 'react';
import { useHistory } from "react-router-dom";
import './styles.css';
import { Form, Field } from 'react-final-form'
import { LoginCredentials } from './types';
import { useAuth } from '../../contexts/Auth';

const validate = (values: LoginCredentials) => {
  const errors  = {} as LoginCredentials;
  if (!values.username) {
    errors.username = 'Required';
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  return errors;
}

const LoginPage = () => {
  const auth = useAuth();
  const history = useHistory();

  const onSubmit = (values: LoginCredentials) => {
    auth.login(values);
    history.push('/feed');
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={({
        submitError,
        handleSubmit,
        submitting,
      }) => (
        <form onSubmit={handleSubmit} className="login-form">
          <h2>Log in form</h2>
          <Field name="username">
            {({ input, meta }) => (
              <div className="input-container">
                <label>Username</label>
                <input {...input} type="text" placeholder="username" />
                {(meta.error || (meta.submitError && !meta.dirtySinceLastSubmit)) && meta.touched && (
                  <span>{meta.error || meta.submitError}</span>
                )}
              </div>
            )}
          </Field>

          <Field name="password">
            {({ input, meta }) => (
              <div className="input-container">
                <label>Password</label>
                <input {...input} type="password" placeholder="Password" />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          {submitError && <div className="error">{submitError}</div>}
          <div className="buttons">
            <button type="submit" disabled={submitting}>
              Log In
            </button>
          </div>
        </form>
      )}
    />
  );
};


export default LoginPage;
