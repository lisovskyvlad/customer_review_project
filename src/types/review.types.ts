export interface ReviewThemeType {
  theme_id: number;
  sentiment: -1 | 0 | 1
}

export interface ReviewType {
  id: number;
  created_at: string;
  comment: string;
  themes: Array<ReviewThemeType>
}

const reviews: Array<ReviewType> = [
  {
    "id": 59458292,
    "created_at": "2019-07-18T23:28:36Z",
    "comment": "exelent",
    "themes": [
      {
        "theme_id": 6372,
        "sentiment": 0
      }
    ]
  },
  {
    "id": 59457786,
    "created_at": "2019-07-18T23:18:53Z",
    "comment": "Excellent keep it up",
    "themes": [
      {
        "theme_id": 6374,
        "sentiment": 1
      }
    ]
  },
  {
    "id": 59457787,
    "created_at": "2019-07-18T23:10:33Z",
    "comment": "Great app, so quick easy to use, I would definitely recommend for hassle free and speedy banking",
    "themes": [
      {
        "theme_id": 6374,
        "sentiment": 1
      },
      {
        "theme_id": 6344,
        "sentiment": 1
      },
      {
        "theme_id": 6345,
        "sentiment": 1
      }
    ]
  },
  {
    "id": 59457788,
    "created_at": "2019-07-18T22:40:21Z",
    "comment": "very easy to use. saves having to keep going to either the cashpoint or bank counter",
    "themes": [
      {
        "theme_id": 6345,
        "sentiment": 1
      }
    ]
  }
];
