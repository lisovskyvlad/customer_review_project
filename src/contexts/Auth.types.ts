export interface CredentialsType {
  username: string;
  password: string;
}

export interface AuthContextProps {
  client: {
    login: (credentials: CredentialsType) => Promise<any>
  }
}

export interface AuthContextValues {
  login: (credentials: CredentialsType) => void;
  // refreshSessionTokens: () => void;
  jwtToken: string;
}
