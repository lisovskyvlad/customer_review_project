// useEffect,
import React, { createContext , FunctionComponent, useContext, useState } from "react";
import { AuthContextProps, AuthContextValues, CredentialsType } from './Auth.types';

const AuthContext = createContext({} as AuthContextValues);

const JWT_ATTRIBUTE = 'jwtToken';

export const AuthProvider: FunctionComponent<AuthContextProps> = ({ children, client }) => {
  const [jwtToken, setJwt] = useState(localStorage.getItem(JWT_ATTRIBUTE) || '');

  const login = async (credentials: CredentialsType) => {
    const response = await client.login(credentials);
    localStorage.setItem(JWT_ATTRIBUTE, response.token);
    setJwt(response.token);
  };

  return (
    <AuthContext.Provider value={{ login, jwtToken }}>
      {children}
    </AuthContext.Provider>
  )
};

export const useAuth = (): AuthContextValues => useContext<AuthContextValues>(AuthContext);
