import axios from 'axios';
import { CHATTERMILL_HOST } from './constants';

const PATH = "login";

export interface loginParametersType {
  username: string;
  password: string;
};

const callLoginEndpoint = async (credentials: loginParametersType) => {
  const response = await axios(
    {
      method: 'post',
      url: `${CHATTERMILL_HOST}/${PATH}`,
      data: credentials
    }
  );

  return response.data;
};

export default callLoginEndpoint;
