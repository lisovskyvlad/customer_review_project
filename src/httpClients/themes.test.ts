import axios from 'axios';
import callThemeEndpoint, { themeCallType } from './themes';

jest.mock('axios');

describe('callThemeEndpoint', () => {
  test('should post username credentials', () => {
    const clientInput: themeCallType = {
      token: 'abc',
      id: 1
    };

    const response = {
      data: [{
        "id": 1,
        "name": "string"
      }]
    };

    axios.mockImplementation(() => Promise.resolve(response))

    return callThemeEndpoint(clientInput).then(data => expect(data).toEqual(response.data));
  });
});
