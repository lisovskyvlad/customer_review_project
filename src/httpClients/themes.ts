import axios from 'axios';
import { CHATTERMILL_HOST } from './constants';
import { ThemeType } from '../types/theme.types';

const PATH = "api/themes";

export interface themeCallType {
  id: number;
  token: string;
}

const callThemeEndpoint = async (
  { id, token }: themeCallType
): Promise<{ data: ThemeType }> => {
  const headers = { Authorization: `Bearer ${token}` };

  const response = await axios({
    method: 'get',
    url: `${CHATTERMILL_HOST}/${PATH}/${id}`,
    headers
  });

  return response.data;
}

export default callThemeEndpoint;
