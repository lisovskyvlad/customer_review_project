import axios from 'axios';
import callLoginEndpoint, { loginParametersType }  from './login';

jest.mock('axios');

test('should post username credentials', () => {
  const usernamePassword: loginParametersType = {
    username: "john@doe.com",
    password: "password"
  };

  const response = {
    data: {
      token: 'abc'
    }
  };

  axios.mockImplementation(() => Promise.resolve(response))

  return callLoginEndpoint(usernamePassword).then(data => expect(data).toEqual(response.data));
});
