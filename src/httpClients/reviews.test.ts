import axios from 'axios';
import callReviewsEndpoint, { reviewsCallType }  from './reviews';

jest.mock('axios');

test('returns reviews', () => {
  const clientInput: reviewsCallType = {
    token: 'abc',
    limit: 10,
    offset: 10
  };

  const response = {
    data: [
      {
        "comment": "string",
        "created_at": "string",
        "id": 0,
        "themes": [
          {
            "sentiment": 0,
            "theme_id": 0
          }
        ]
      }
    ]
  };

  axios.mockImplementation(() => Promise.resolve(response))

  return callReviewsEndpoint(clientInput).then(data => expect(data).toEqual(response.data));
});
