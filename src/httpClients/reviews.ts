import axios from 'axios';
import { CHATTERMILL_HOST } from './constants';
import { ReviewType } from '../types/review.types';

const PATH = "api/reviews";

export interface reviewsCallType {
  token: string;
  limit: number;
  offset: number;
}

const callReviewsEndpoint = async ({
  token, limit = 20, offset = 0
}: reviewsCallType): Promise<{ data: Array<ReviewType> }> => {
  const headers = { Authorization: `Bearer ${token}` };

  const response = await axios(
    {
      method: 'get',
      url: `${CHATTERMILL_HOST}/${PATH}`,
      headers,
      params: { limit, offset }
    }
  );

  return response.data;
};

export default callReviewsEndpoint;
